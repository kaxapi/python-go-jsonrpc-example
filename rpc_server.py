import json
import traceback
from json.decoder import JSONDecodeError
import socket
from logging import getLogger

import sys


class RPCServer:
    """
    Listens to incoming connections & data, performs JSON RPC
    """

    def __init__(self, port: int, cb: callable, error_cb: callable = None, buffer_size: int = 8192,
                 backlog_size: int = 100):
        """
        initializes class and starts listening for incoming JSON RPC calls
        :param port: port to bind
        :param cb: callback for incoming messages, see http://www.jsonrpc.org/specification#request_object
        :param error_cb: error callback, arguments: raw response (str)
        :param buffer_size: buffer size for recv
        :param backlog_size: queue size for incoming data
        """

        logger = getLogger("RPCServer")
        self.logger = logger

        # setup socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(("0.0.0.0", port))
        self.socket.listen(backlog_size)

        logger.info("Bound to port %d" % port)
        connect = None
        params = None
        # start accepting connections
        connect, address = self.socket.accept()

        while True:
            try:

                # wait for data
                logger.info("waiting for data")
                response = connect.recv(buffer_size)

                if response:
                    try:
                        # load JSON
                        params = json.loads(response.decode())
                        logger.info("requested '{}' method, with params {}".format(params.get("method"),
                                                                                   params.get("params")))

                        # call the callback function
                        data = cb(params)

                        logger.info("result: {}".format(data))

                        # compose a result
                        result = {"result": data,
                                  "id": params.get("id")}

                        # send
                        connect.send(json.dumps(result).encode())
                    except JSONDecodeError:
                        logger.error("JSONDecodeError while decoding response")

                        # call error callback function
                        if error_cb:
                            error_cb(response)

                        # compose error response
                        err = {"code": -32700,  # parse error
                               "message": "JSON decode error"}
                        data = {"error": err,
                                "id": params.get("id")}
                        connect.send(json.dumps(data).encode())
                else:
                    logger.warning("connection interrupted, accepting new connections...")
                    connect, address = self.socket.accept()

            except ConnectionResetError:
                logger.warning(
                    "An existing connection was forcibly closed by the remote host, accepting new connections...")
                connect, address = self.socket.accept()

            except KeyboardInterrupt:
                break

            except Exception as e:
                logger.error(e)
                traceback.print_exc()
                if connect is not None and params is not None:
                    try:

                        # compose error response
                        err = {"code": -32603,  # internal error
                               "message": "py service internal error, see logs"}

                        data = {"error": err,
                                "id": params.get("id")}

                        connect.send(json.dumps(data).encode())
                    except Exception as e:
                        # terminate in case of failed error response
                        logger.error("Got exception during sending an error {}".format(e))
                        sys.exit(1)
