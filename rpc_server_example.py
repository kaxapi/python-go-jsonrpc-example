from pprint import pprint
import logging

from rpc_server import RPCServer


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s: %(levelname)s %(message)s')


def result(data) -> str:
    pprint(data)
    return "well, hello there!"


RPCServer(17666, result)