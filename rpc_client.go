package main

import (
	"fmt"
	"net"
	"net/rpc/jsonrpc"
	"os"
)

func main() {
	conn, e := net.Dial("tcp", "localhost:17666")
	if e != nil {
		fmt.Fprintf(os.Stderr, "Could not connect: %s\n", e)
		os.Exit(1)
	}
	defer conn.Close()

	client := jsonrpc.NewClient(conn)
	var reply string
	arg := "hello!"
	fmt.Printf("Sending: %s\n", arg)
	client.Call("RPCFunc.Echo", arg, &reply)
	fmt.Printf("Reply: %s\n", reply)
}
